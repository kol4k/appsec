<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Website Digital Talent Scholarship Kementerian Komunikasi dan Informatika RI">
    <meta name="keywords" content="Digital Talent Scholarship, Digital Talent, Digitalent, Kementerian, Kominfo, Informatika, Sertifikasi, Bimtek, Bimbingan Teknis, SKKNI, Literasi, SDM, Indonesia, Proserti, Litprofkom, Litprofinformatika, DTS, Scholarship">
    <meta name="author" content="Digital Talent Scholarship 2020">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="color:Background" content="#f4fbfa">
    <meta property="og:url" content="{{ url('/') }}">
    <meta property="og:type" content="article">
    <meta property="og:title" content="Digital Talent Scholarship 2020">
    <meta property="og:description" content="Digital Talent Scholarship Kementerian Komunikasi dan Informatika">
    <meta property="og:site_name" content="Website Digital Talent Scholarship Kementerian Komunikasi dan Informatika RI">

    <title>@yield('title', 'AppSec.Asia')</title>

    <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon-16x16.png') }}">
    <link rel="apple-touch-icon" href="{{ asset('apple-touch-icon.png') }}" />
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('apple-touch-icon.png') }}" />
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('apple-touch-icon.png') }}" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('apple-touch-icon.png') }}" />
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('apple-touch-icon.png') }}" />
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('apple-touch-icon.png') }}" />
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('apple-touch-icon.png') }}" />
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('apple-touch-icon.png') }}" />
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('apple-touch-icon.png') }}">

    <!-- Vendor Styles-->
    <link rel="stylesheet" media="screen" href="{{ asset('assets/vendor/lightgallery.js/dist/css/lightgallery.min.css') }}" />
    <link rel="stylesheet" media="screen" href="{{ asset('assets/vendor/simplebar/dist/simplebar.min.css') }}" />
    <link rel="stylesheet" media="screen" href="{{ asset('assets/vendor/tiny-slider/dist/tiny-slider.css') }}" />

    <!-- Main Theme Styles + Bootstrap-->
    <link rel="stylesheet" media="screen" href="{{ asset('assets/css/theme.css') }}">
</head>

<!-- Body-->

<body>
    <!-- Main content-->
    <!-- Wraps everything except footer to push footer to the bottom of the page if there is little content -->
    <main class="cs-page-wrapper position-relative">

        <!-- Navbar -->
        <!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page -->
        <header class="cs-header navbar navbar-expand-lg navbar-light navbar-sticky">
            <div class="container px-0 px-xl-3">
                <a class="navbar-brand order-lg-1 mr-lg-5 mr-0 pr-lg-2" href="{{ Route('home') }}">
                    <img src="{{ asset('assets/@images/logo.orig.png') }}" alt="Createx Logo" width="130">
                </a>
                @guest
                <div class="d-flex align-items-center order-lg-3">
                    <a class="btn btn-primary btn-hover-shadow d-sm-inline-block d-none ml-4" href="javascript::void();">
                        <i class="cxi-profile mt-n1 mr-2 lead align-middle"></i>
                        Masuk / Daftar
                    </a>
                    <button class="navbar-toggler ml-1 mr-n3" type="button" data-toggle="collapse" data-target="#navbarCollapse1" aria-expanded="false">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>
                @else
                <div class="d-flex align-items-center order-lg-3">
                    <div class="dropdown">
                        <a href="#" class="text-dark dropdown-toggle d-lg-inline-block d-none ml-4 pl-1 text-decoration-none text-nowrap" data-toggle="dropdown">
                            <i class="cxi-profile mr-1 font-size-base align-middle"></i>
                            {{ Auth::user()->name }}
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="javascript::void();" class="dropdown-item d-flex align-items-center">
                                <i class="cxi-profile font-size-base mr-2"></i>
                                <span>Akun Saya</span>
                            </a>
                            <a href="javascript::void();" class="dropdown-item d-flex align-items-center">
                                <i class="cxi-list font-size-base mr-2"></i>
                                <span>Pelatihan</span>
                                <span class="badge badge-success ml-auto">2</span>
                            </a>
                            <a href="account-reviews.html" class="dropdown-item d-flex align-items-center">
                                <i class="cxi-settings font-size-base mr-2"></i>
                                <span>Pengaturan</span>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="#" class="dropdown-item d-flex align-items-center">
                                <i class="cxi-logout font-size-base mr-2"></i>
                                <span>Keluar</span>
                            </a>
                        </div>
                    </div>
                    <button class="navbar-toggler ml-1 mr-n3" type="button" data-toggle="collapse" data-target="#navbarCollapse1" aria-expanded="false">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>
                @endif
                <nav class="collapse navbar-collapse order-lg-2" id="navbarCollapse1">
                    <ul class="navbar-nav mr-auto text-nowrap">
                        <li class="nav-item">
                            <a class="nav-link" href="javascript::void();">Home</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Services</a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a class="dropdown-item" href="javascript::void();">Website Development</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="javascript::void();">Security Assesment</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="javascript::void();">Cyber Security Insident Response</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="javascript::void();">Digital Forensic Investigation</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Training</a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a class="dropdown-item" href="events-list.html">Panduan Tes Substansi</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="events-grid.html">Hak & Kewajiban</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="javascript::void();">Rilis Media</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="contacts.html">Kontak</a>
                        </li>
                        <li class="nav-item d-sm-none d-block">
                            <a class="btn btn-sm btn-gradient btn-hover-shadow m-3" href="javascript::void();">Masuk / Daftar</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </header>

        @yield('content')
    </main>


    <!-- Footer -->
    <footer class="cs-footer pt-sm-5 pt-4 bg-dark">
        <div class="container py-3">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-6 order-lg-1 order-1 mb-lg-0 mb-4">
                    <a class="d-inline-block mb-4" href="{{ Route('home') }}">
                        <img src="{{ asset('assets/@images/logo-light.png') }}" width="130" alt="Createx logo">
                    </a>
                    <p class="mb-sm-4 mb-3 pb-lg-3 font-size-xs text-light opacity-60">Createx Online School is a leader in online studying. We have lots of courses and programs from the main market experts. We provide relevant approaches to online learning, internships and employment in the largest companies in the country.</p>
                    <a href="#" class="social-btn mr-1 mb-2 mt-md-0 mt-sm-1">
                        <i class="cxi-facebook"></i>
                    </a>
                    <a href="#" class="social-btn mr-1 mb-2 mt-md-0 mt-sm-1">
                        <i class="cxi-twitter"></i>
                    </a>
                    <a href="#" class="social-btn mr-1 mb-2 mt-md-0 mt-sm-1">
                        <i class="cxi-youtube"></i>
                    </a>
                    <a href="#" class="social-btn mr-1 mb-2 mt-md-0 mt-sm-1">
                        <i class="cxi-telegram"></i>
                    </a>
                    <a href="#" class="social-btn mr-1 mb-2 mt-md-0 mt-sm-1">
                        <i class="cxi-instagram"></i>
                    </a>
                    <a href="#" class="social-btn mb-1 mt-md-0 mt-sm-1">
                        <i class="cxi-linkedin"></i>
                    </a>
                </div>
                <div class="col-lg-2 col-sm-12 col-6 order-lg-2 order-sm-4 order-1 mb-lg-0 mb-4">
                    <h3 class="h6 mb-2 pb-1 text-uppercase text-light">Site map</h3>
                    <ul class="nav nav-light flex-lg-column flex-sm-row flex-column">
                        <li class="nav-item mb-2">
                            <a href="about.html" class="nav-link mr-lg-0 mr-sm-4 p-0 font-weight-normal">About Us</a>
                        </li>
                        <li class="nav-item mb-2">
                            <a href="courses.html" class="nav-link mr-lg-0 mr-sm-4 p-0 font-weight-normal">Courses</a>
                        </li>
                        <li class="nav-item mb-2">
                            <a href="events.html" class="nav-link mr-lg-0 mr-sm-4 p-0 font-weight-normal">Events</a>
                        </li>
                        <li class="nav-item mb-2">
                            <a href="blog.html" class="nav-link mr-lg-0 mr-sm-4 p-0 font-weight-normal">Blog</a>
                        </li>
                        <li class="nav-item mb-2">
                            <a href="contacts.html" class="nav-link mr-lg-0 mr-sm-4 p-0 font-weight-normal">Contacts</a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-2 col-sm-12 col-6 order-lg-3 order-sm-5 order-2 mb-lg-0 mb-4">
                    <h3 class="h6 mb-2 pb-1 text-uppercase text-light">Courses</h3>
                    <ul class="nav nav-light flex-lg-column flex-sm-row flex-column">
                        <li class="nav-item mb-2">
                            <a href="courses.html" class="nav-link mr-lg-0 mr-sm-4 p-0 font-weight-normal">Marketing</a>
                        </li>
                        <li class="nav-item mb-2">
                            <a href="courses.html" class="nav-link mr-lg-0 mr-sm-4 p-0 font-weight-normal">Management</a>
                        </li>
                        <li class="nav-item mb-2">
                            <a href="courses.html" class="nav-link mr-lg-0 mr-sm-4 p-0 font-weight-normal">HR &amp; Recruting</a>
                        </li>
                        <li class="nav-item mb-2">
                            <a href="courses.html" class="nav-link mr-lg-0 mr-sm-4 p-0 font-weight-normal">Design</a>
                        </li>
                        <li class="nav-item mb-2">
                            <a href="courses.html" class="nav-link mr-lg-0 mr-sm-4 p-0 font-weight-normal">Development</a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-4 order-lg-4 order-sm-3 order-3 mb-md-0 mb-4">
                    <h3 class="h6 mb-2 pb-1 text-uppercase text-light">Contact us</h3>
                    <ul class="nav nav-light flex-md-column flex-sm-row flex-column">
                        <li class="nav-item mb-2">
                            <a href="tel:(405)555-0128" class="nav-link mr-md-0 mr-sm-4 p-0 font-weight-normal text-nowrap">
                                <i class="cxi-iphone mr-2"></i>
                                (405) 555-0128
                            </a>
                        </li>
                        <li class="nav-item mb-2">
                            <a href="mailto:hello@example.com" class="nav-link mr-md-0 mr-sm-4 p-0 font-weight-normal text-nowrap">
                                <i class="cxi-chat mr-2"></i>
                                hello@example.com
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 order-lg-5 order-sm-2 order-4 mb-sm-0 mb-4">
                    <h3 class="h6 mb-2 pb-1 text-uppercase text-light">Sign up to our newsletter</h3>
                    <form>
                        <div class="input-group-overlay input-group-light form-group mb-2 pb-1">
                            <input class="form-control appended-form-control" type="text" placeholder="Email address*">
                            <div class="input-group-append-overlay">
                                <span class="input-group-text">
                                    <button class="btn btn-link bt-sm px-0" type="submit">
                                        <i class="cxi-arrow-right lead mt-n1"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                        <small class="form-text font-size-xxs text-light">*Subscribe to our newsletter to receive early discount offers, updates and new products info.</small>
                    </form>
                </div>
            </div>
        </div>
        <div style="background-color: #292C37;">
            <div class="container py-2">
                <div class="d-flex align-items-sm-center justify-content-between py-1">
                    <div class="text-light">
                        <span class="d-sm-inline d-block mb-1">
                            Hak Cipta
                            <span class="font-size-sm">&copy;
                            </span>
                            {{ \Carbon\Carbon::now()->format('Y') }} | Kementerian Komunikasi dan Informatika
                        </span>
                    </div>
                    <div class="d-flex align-items-sm-center">
                        <span class="d-sm-inline-block d-none text-light font-size-sm mr-3 mb-1 align-vertical">Kembali ke atas</span>
                        <a class="btn-scroll-top position-static rounded" href="#top" data-scroll>
                            <i class="btn-scroll-top-icon cxi-angle-up"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Vendor scripts: js libraries and plugins-->
    <script src="{{ asset('assets/vendor/jquery/dist/jquery.slim.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/lightgallery.js/dist/js/lightgallery.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/lg-video.js/dist/lg-video.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/jarallax/dist/jarallax.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/simplebar/dist/simplebar.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/tiny-slider/dist/min/tiny-slider.js') }}"></script>

    <!-- Main theme script-->
    <script src="{{ asset('assets/js/theme.min.js') }}"></script>
</body>

</html>