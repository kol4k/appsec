<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="img/favicon.png">
    <title>AppSec.Asia</title>
    <link rel="stylesheet" href="css/plugins.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/colors/navy.css">
</head>

<body>
    <div class="content-wrapper">
        <header class="wrapper bg-dark">
            <nav class="navbar center-nav transparent navbar-expand-lg navbar-dark">
                <div class="container flex-lg-row flex-nowrap align-items-center">
                    <div class="navbar-brand w-100">
                        <a href="/">
                            <h5 class="logo-dark text-info mt-2">AppSec.Asia</h5>
                            <h5 class="logo-light text-info mt-2">AppSec.Asia</h5>
                        </a>
                    </div>
                    <div class="navbar-collapse offcanvas-nav">
                        <div class="offcanvas-header d-lg-none d-xl-none">
                            <a href="/"><img src="img/logo-light.png" srcset="img/logo-light@2x.png 2x" alt="" /></a>
                            <button type="button" class="btn-close btn-close-white offcanvas-close offcanvas-nav-close" aria-label="Close"></button>
                        </div>
                        <ul class="navbar-nav">
                            <li class="nav-item"><a class="nav-link" href="#!">Home</a></li>
                            <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#!">Application Development</a>
                                <ul class="dropdown-menu">
                                    <li class="nav-item"><a class="dropdown-item" href="projects.html">Link 1</a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#!">Cyber Security</a>
                                <ul class="dropdown-menu">
                                    <li class="nav-item"><a class="dropdown-item" href="javascript::void();">Link 1</a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#!">Training</a>
                                <ul class="dropdown-menu">
                                    <li class="nav-item"><a class="dropdown-item" href="javascript::void();">Link 1</a></li>
                                </ul>
                            </li>
                            <li class="nav-item"><a class="nav-link" href="#!">Contact</a></li>
                        </ul>
                        <!-- /.navbar-nav -->
                    </div>
                    <!-- /.navbar-collapse -->
                    <div class="navbar-other w-100 d-flex ms-auto">
                        <ul class="navbar-nav flex-row align-items-center ms-auto" data-sm-skip="true">
                            <li class="nav-item dropdown language-select text-uppercase">
                                <a class="nav-link dropdown-item dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">En</a>
                                <ul class="dropdown-menu">
                                    <li class="nav-item"><a class="dropdown-item" href="#">En</a></li>
                                    <li class="nav-item"><a class="dropdown-item" href="#">De</a></li>
                                    <li class="nav-item"><a class="dropdown-item" href="#">Es</a></li>
                                </ul>
                            </li>
                            <li class="nav-item d-lg-none">
                                <div class="navbar-hamburger"><button class="hamburger animate plain" data-toggle="offcanvas-nav"><span></span></button></div>
                            </li>
                        </ul>
                        <!-- /.navbar-nav -->
                    </div>
                    <!-- /.navbar-other -->
                </div>
                <!-- /.container -->
            </nav>
            <!-- /.navbar -->
        </header>
        <!-- /header -->
        <section class="wrapper bg-dark angled lower-start">
            <div class="container pt-7 pt-md-11 pb-8">
                <div class="row gx-0 gy-10 align-items-center">
                    <div class="col-lg-6" data-cues="slideInDown" data-group="page-title" data-delay="600">
                        <h1 class="display-1 text-white mb-4 text-white"><span class="text-info">AppSec</span> focuses on <br /><span class="typer text-warning text-nowrap" data-delay="100" data-delim=":" data-words="customer satisfaction:business needs:creative ideas"></span><span class="cursor text-primary" data-owner="typer"></span></h1>
                        <p class="lead fs-24 lh-sm text-white mb-7 pe-md-18 pe-lg-0 pe-xxl-15">We carefully consider our solutions to support each and every stage of your growth.</p>
                    </div>
                    <!-- /column -->
                    <div class="col-lg-5 offset-lg-1 mb-n18" data-cues="slideInDown">
                        <div class="position-relative light-gallery-wrapper">
                            <figure class="rounded shadow-lg"><img src="img/photos/about13.jpg" srcset="img/photos/about13@2x.jpg 2x" alt=""></figure>
                        </div>
                        <!-- /div -->
                    </div>
                    <!-- /column -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </section>
        <!-- /section -->
        <section class="wrapper bg-light">
            <div class="container pt-19 pt-md-21 pb-16 pb-md-18">
                <div class="card shadow-none my-n15 my-lg-n17">
                    <div class="card-body py-12 py-lg-14 px-lg-11 py-xl-16 px-xl-13">
                        <div class="row text-center">
                            <div class="col-lg-9 col-xl-8 col-xxl-7 mx-auto">
                                <h2 class="fs-15 text-uppercase text-muted mb-3">What We Do?</h2>
                                <h3 class="display-4 mb-9">The service we offer is</h3>
                            </div>
                            <!-- /column -->
                        </div>
                        <!-- /.row -->
                        <div class="row gx-md-8 gx-xl-12 gy-8 mb-14 mb-md-16 text-center">
                            <div class="col-md-4">
                                <div class="icon btn btn-block btn-lg btn-soft-purple disabled mb-5"> <i class="uil uil-window-restore"></i> </div>
                                <h4>Application Development</h4>
                                <p class="mb-3">We design beautiful, and conversion driven websites that represent your brand well to engage with your customers.</p>
                                <a href="#" class="more hover link-purple">Learn More</a>
                            </div>
                            <!--/column -->
                            <div class="col-md-4">
                                <div class="icon btn btn-block btn-lg btn-soft-green disabled mb-5"> <i class="uil uil-shield-exclamation"></i> </div>
                                <h4>Cyber Security</h4>
                                <p class="mb-3">Full cycle application security consulting services and penetration testing – from design to production.</p>
                                <a href="#" class="more hover link-green">Learn More</a>
                            </div>
                            <!--/column -->
                            <div class="col-md-4">
                                <div class="icon btn btn-block btn-lg btn-soft-orange disabled mb-5"> <i class="uil uil-laptop-cloud"></i> </div>
                                <h4>Training</h4>
                                <p class="mb-3">High-end, hands on, training in secure coding and penetration testing on a variety of platforms.</p>
                                <a href="#" class="more hover link-orange">Learn More</a>
                            </div>
                            <!--/column -->
                        </div>
                        <hr class="my-14 my-md-16" />
                        <!--/.row -->
                        <div class="row gy-10 gx-8 gx-lg-12 my-14 mb-md-16 align-items-center">
                            <div class="col-lg-6 position-relative">
                                <div class="shape bg-dot primary rellax w-17 h-21" data-rellax-speed="1" style="top: -2rem; left: -1.9rem;"></div>
                                <div class="shape bg-dot primary rellax w-17 h-21" data-rellax-speed="1" style="bottom: -1.8rem; right: -0.5rem;"></div>
                                <figure class="rounded"><img src="img/introduction-to-cybersecurity.jpg" alt="" /></figure>
                            </div>
                            <!--/column -->
                            <div class="col-lg-6">
                                <div class="row gx-xl-10 gy-6" data-cues="slideInUp" data-group="services">
                                    <div class="col-lg-12">
                                        <div class="d-flex flex-row">
                                            <div>
                                                <div class="icon btn btn-circle btn-lg btn-soft-primary disabled me-5"> <i class="uil uil-lightbulb-alt"></i> </div>
                                            </div>
                                            <div>
                                                <h4 class="mb-1">Our Mission</h4>
                                                <p class="mb-0">Our mission is to raise awareness in the software development world to the importance of integrating software security across the development lifecycle.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/column -->
                        </div>
                        <!--/.row -->
                        <!-- /.row -->
                        <hr class="my-14 my-md-16" />
                        <h2 class="fs-15 text-uppercase text-info mb-3">Our Events</h2>
                        <h3 class="display-4 mb-7">Seminar and Workshop</h3>
                        <div class="carousel owl-carousel blog grid-view mb-18" data-margin="30" data-dots="true" data-autoplay="false" data-autoplay-timeout="5000" data-responsive='{"0":{"items": "1"}, "768":{"items": "2"}, "992":{"items": "2"}, "1200":{"items": "3"}}'>
                            <div class="item">
                                <article>
                                    <figure class="overlay overlay1 hover-scale rounded mb-6"><a href="#"> <img src="img/photos/b4.jpg" alt="" /></a>
                                        <figcaption>
                                            <h5 class="from-top mb-0">Read More</h5>
                                        </figcaption>
                                    </figure>
                                    <div class="post-header">
                                        <h2 class="post-title h3 mb-3"><a class="link-dark" href="javascript::void();">Changed responsibilities in modern software development environments – OWASP Jakarta Webinar</a></h2>
                                    </div>
                                    <!-- /.post-header -->
                                    <div class="post-footer">
                                        <ul class="post-meta">
                                            <li class="post-date"><i class="uil uil-calendar-alt"></i><span>14 Apr 2021</span></li>
                                            <li class="post-comments"><a href="#"><i class="uil uil-file-alt fs-15"></i>Coding</a></li>
                                        </ul>
                                        <!-- /.post-meta -->
                                    </div>
                                    <!-- /.post-footer -->
                                </article>
                                <!-- /article -->
                            </div>
                            <!-- /.item -->
                            <div class="item">
                                <article>
                                    <figure class="overlay overlay1 hover-scale rounded mb-6"><a href="#"> <img src="img/photos/b5.jpg" alt="" /></a>
                                        <figcaption>
                                            <h5 class="from-top mb-0">Read More</h5>
                                        </figcaption>
                                    </figure>
                                    <div class="post-header">
                                        <h2 class="post-title h3 mb-3"><a class="link-dark" href="javascript::void();">Using OWASP Nettacker For Recon and Vulnerability Scanning – OWASP Jakarta Webinar</a></h2>
                                    </div>
                                    <!-- /.post-header -->
                                    <div class="post-footer">
                                        <ul class="post-meta">
                                            <li class="post-date"><i class="uil uil-calendar-alt"></i><span>29 Mar 2021</span></li>
                                            <li class="post-comments"><a href="#"><i class="uil uil-file-alt fs-15"></i>Workspace</a></li>
                                        </ul>
                                        <!-- /.post-meta -->
                                    </div>
                                    <!-- /.post-footer -->
                                </article>
                                <!-- /article -->
                            </div>
                            <!-- /.item -->
                            <div class="item">
                                <article>
                                    <figure class="overlay overlay1 hover-scale rounded mb-6"><a href="#"> <img src="img/photos/b6.jpg" alt="" /></a>
                                        <figcaption>
                                            <h5 class="from-top mb-0">Read More</h5>
                                        </figcaption>
                                    </figure>
                                    <div class="post-header">
                                        <h2 class="post-title h3 mb-3"><a class="link-dark" href="javascript::void();">OWASP Devsecops Maturity Model</a></h2>
                                    </div>
                                    <!-- /.post-header -->
                                    <div class="post-footer">
                                        <ul class="post-meta">
                                            <li class="post-date"><i class="uil uil-calendar-alt"></i><span>26 Feb 2021</span></li>
                                            <li class="post-comments"><a href="#"><i class="uil uil-file-alt fs-15"></i>Meeting</a></li>
                                        </ul>
                                        <!-- /.post-meta -->
                                    </div>
                                    <!-- /.post-footer -->
                                </article>
                                <!-- /article -->
                            </div>
                            <!-- /.item -->
                            <div class="item">
                                <article>
                                    <figure class="overlay overlay1 hover-scale rounded mb-6"><a href="#"> <img src="img/photos/b7.jpg" alt="" /></a>
                                        <figcaption>
                                            <h5 class="from-top mb-0">Read More</h5>
                                        </figcaption>
                                    </figure>
                                    <div class="post-header">
                                        <h2 class="post-title h3 mb-3"><a class="link-dark" href="javascript::void();">Morbi leo risus porta eget</a></h2>
                                    </div>
                                    <div class="post-footer">
                                        <ul class="post-meta">
                                            <li class="post-date"><i class="uil uil-calendar-alt"></i><span>7 Jan 2021</span></li>
                                            <li class="post-comments"><a href="#"><i class="uil uil-file-alt fs-15"></i>Business Tips</a></li>
                                        </ul>
                                        <!-- /.post-meta -->
                                    </div>
                                    <!-- /.post-footer -->
                                </article>
                                <!-- /article -->
                            </div>
                            <!-- /.item -->
                        </div>
                        <h2 class="fs-15 text-uppercase text-info mb-3">Our Private Events</h2>
                        <h3 class="display-4 mb-7">Training and Certification</h3>
                        <div class="carousel owl-carousel blog grid-view mb-18" data-margin="30" data-dots="true" data-autoplay="false" data-autoplay-timeout="5000" data-responsive='{"0":{"items": "1"}, "768":{"items": "2"}, "992":{"items": "2"}, "1200":{"items": "3"}}'>
                            <div class="item">
                                <article>
                                    <figure class="overlay overlay1 hover-scale rounded mb-6"><a href="#"> <img src="img/computer.jpg" alt="" /></a>
                                        <figcaption>
                                            <h5 class="from-top mb-0">Read More</h5>
                                        </figcaption>
                                    </figure>
                                    <div class="post-header">
                                        <h2 class="post-title h3 mb-3"><a class="link-dark" href="javascript::void();">CompTIA IT Fundamentals (ITF+)</a></h2>
                                    </div>
                                    <!-- /.post-header -->
                                    <div class="post-footer">
                                        <ul class="post-meta">
                                            <li class="post-date text-danger"><i class="uil uil-university"></i><span>CompTIA</span></li>
                                            <li class="post-date text-success"><i class="uil uil-pricetag-alt fs-15"></i>Rp. 9.000.000</a></li>
                                        </ul>
                                        <!-- /.post-meta -->
                                    </div>
                                    <!-- /.post-footer -->
                                </article>
                                <!-- /article -->
                            </div>
                            <!-- /.item -->
                            <div class="item">
                                <article>
                                    <figure class="overlay overlay1 hover-scale rounded mb-6"><a href="#"> <img src="img/computer.jpg" alt="" /></a>
                                        <figcaption>
                                            <h5 class="from-top mb-0">Read More</h5>
                                        </figcaption>
                                    </figure>
                                    <div class="post-header">
                                        <h2 class="post-title h3 mb-3"><a class="link-dark" href="javascript::void();">CompTIA A+</a></h2>
                                    </div>
                                    <!-- /.post-header -->
                                    <div class="post-footer">
                                        <ul class="post-meta">
                                            <li class="post-date text-danger"><i class="uil uil-university"></i><span>CompTIA</span></li>
                                            <li class="post-date text-success"><i class="uil uil-pricetag-alt fs-15"></i>Rp. 9.000.000</a></li>
                                        </ul>
                                        <!-- /.post-meta -->
                                    </div>
                                    <!-- /.post-footer -->
                                </article>
                                <!-- /article -->
                            </div>
                            <!-- /.item -->
                            <div class="item">
                                <article>
                                    <figure class="overlay overlay1 hover-scale rounded mb-6"><a href="#"> <img src="img/computer.jpg" alt="" /></a>
                                        <figcaption>
                                            <h5 class="from-top mb-0">Read More</h5>
                                        </figcaption>
                                    </figure>
                                    <div class="post-header">
                                        <h2 class="post-title h3 mb-3"><a class="link-dark" href="javascript::void();">CompTIA Security+</a></h2>
                                    </div>
                                    <!-- /.post-header -->
                                    <div class="post-footer">
                                        <ul class="post-meta">
                                            <li class="post-date text-danger"><i class="uil uil-university"></i><span>CompTIA</span></li>
                                            <li class="post-date text-success"><i class="uil uil-pricetag-alt fs-15"></i>Rp. 9.000.000</a></li>
                                        </ul>
                                        <!-- /.post-meta -->
                                    </div>
                                    <!-- /.post-footer -->
                                </article>
                                <!-- /article -->
                            </div>
                            <!-- /.item -->
                            <div class="item">
                                <article>
                                    <figure class="overlay overlay1 hover-scale rounded mb-6"><a href="#"> <img src="img/photos/b7.jpg" alt="" /></a>
                                        <figcaption>
                                            <h5 class="from-top mb-0">Read More</h5>
                                        </figcaption>
                                    </figure>
                                    <div class="post-header">
                                        <h2 class="post-title h3 mb-3"><a class="link-dark" href="javascript::void();">Morbi leo risus porta eget</a></h2>
                                    </div>
                                    <div class="post-footer">
                                        <ul class="post-meta">
                                            <li class="post-date"><i class="uil uil-calendar-alt"></i><span>7 Jan 2021</span></li>
                                            <li class="post-comments"><a href="#"><i class="uil uil-file-alt fs-15"></i>Business Tips</a></li>
                                        </ul>
                                        <!-- /.post-meta -->
                                    </div>
                                    <!-- /.post-footer -->
                                </article>
                                <!-- /article -->
                            </div>
                            <!-- /.item -->
                        </div>
                        <div class="row gy-10 gy-sm-13 gx-lg-3 mb-16 mb-md-18 align-items-center">
                            <div class="col-lg-4 position-relative">
                                <div class="shape bg-dot primary rellax w-17 h-21" data-rellax-speed="1" style="top: -2rem; left: -1.9rem;"></div>
                                <div class="shape rounded bg-soft-primary rellax d-md-block" data-rellax-speed="0" style="bottom: -1.8rem; right: -1.5rem; width: 85%; height: 90%; "></div>
                                <figure class="rounded"><img src="img/comptia.png" alt="" /></figure>
                            </div>
                            <!--/column -->
                            <div class="col-lg-7 offset-lg-1">
                                <h3 class="display-4 mb-7">We are Part of <span class="text-danger">CompTIA Authorized</span> Partner Program</h3>
                                <p>
                                    <strong>CompTIA</strong> is a non-profit trade association renowned for its certification exams and exam preparation courses for information technology.
                                    <br><br>
                                    More than 1 million IT professionals in the world are certified CompTIA is an excellent IT certification to build an IT career, if you are new to the IT industry then certification will help as your first step in the IT industry, and if you are an IT professional then CompTIA certification can validate your skills and enhance your career.
                                </p>
                            </div>
                            <!--/column -->
                        </div>
                        <!-- /.owl-carousel -->
                        <hr class="my-14 my-md-16" />
                        <!--/.row -->
                        <div class="wrapper image-wrapper bg-auto no-overlay bg-image text-center bg-map" data-image-src="img/map.png">
                            <div class="container py-md-10 py-lg-12">
                                <div class="row">
                                    <div class="col-xl-11 col-xxl-9 mx-auto">
                                        <h3 class="display-4 mb-8 px-lg-8">We are trusted by over 100+ clients. Join them now and grow your business.</h3>
                                    </div>
                                    <!-- /column -->
                                </div>
                                <!-- /.row -->
                                <div class="d-flex justify-content-center">
                                    <span><a class="btn btn-primary rounded-pill">Get a quoted</a></span>
                                </div>
                            </div>
                            <!-- /.container -->
                        </div>
                        <!-- /.wrapper -->
                    </div>
                    <!--/.card-body -->
                </div>
                <!--/.card -->
            </div>
            <!-- /.container -->
        </section>
        <!-- /section -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="bg-dark text-inverse">
        <div class="container pt-20 pt-lg-21 pb-7">
            <div class="row gy-6 gy-lg-0">
                <div class="col-lg-4">
                    <div class="widget">
                        <h3 class="h2 text-white mb-3">Appsec.Asia</h3>
                        <p class="lead mb-5">Our mission is to raise awareness in the software development world to the importance of integrating software security across the development lifecycle.</p>
                        <nav class="nav social social-white">
                            <a href="#"><i class="uil uil-twitter"></i></a>
                            <a href="#"><i class="uil uil-facebook-f"></i></a>
                            <a href="#"><i class="uil uil-instagram"></i></a>
                            <a href="#"><i class="uil uil-youtube"></i></a>
                        </nav>
                    </div>
                    <!-- /.widget -->
                </div>
                <!-- /column -->
                <div class="col-md-4 col-lg-2 offset-lg-2">
                    <div class="widget">
                        <h4 class="widget-title mb-3 text-white">Need Help?</h4>
                        <ul class="list-unstyled mb-0">
                            <li><a href="#">Link 1</a></li>
                            <li><a href="#">Link 2</a></li>
                            <li><a href="#">Link 3</a></li>
                            <li><a href="#">Link 4</a></li>
                        </ul>
                    </div>
                    <!-- /.widget -->
                </div>
                <!-- /column -->
                <div class="col-md-8 col-lg-4">
                    <div class="widget">
                        <h4 class="widget-title mb-3 text-white">Get in Touch</h4>
                        <div class="row">
                            <div class="col-md-6 mb-5">
                                <h5 class="text-white">USA HQ</h5>
                                <address>1423 Broadway Ste 319 Oakland, CA 94612</address>
                                Telp: +1 (415) 799-7722
                            </div>
                            <div class="col-md-6">
                                <h5 class="text-white">Indonesia HQ</h5>
                                <address>AD Premier Office Park 9th Floor, Jakarta 12550</address>
                                Telp: (021) 22708903
                            </div>
                            <div class="col-md-12">
                                <p class="text-white">Monday - Friday: 9:00 - 19:00<br>Closed on Weekends</p>
                            </div>
                        </div>
                    </div>
                    <!-- /.widget -->
                </div>
                <!-- /column -->
            </div>
            <!--/.row -->
            <hr class="mt-13 mt-md-15 mb-7" />
            <div class="d-md-flex align-items-center justify-content-between">
                <p class="mb-2 mb-lg-0">© 2021 AppSec.Asia. All rights reserved.</p>
                <nav class="nav social social-white text-md-end">
                    <a href="#"><i class="uil uil-twitter"></i></a>
                    <a href="#"><i class="uil uil-facebook-f"></i></a>
                    <a href="#"><i class="uil uil-dribbble"></i></a>
                    <a href="#"><i class="uil uil-instagram"></i></a>
                    <a href="#"><i class="uil uil-youtube"></i></a>
                </nav>
                <!-- /.social -->
            </div>
            <!-- /div -->
        </div>
        <!-- /.container -->
    </footer>
    <div class="progress-wrap">
        <svg class="progress-circle svg-content" width="100%" height="100%" viewBox="-1 -1 102 102">
            <path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98" />
        </svg>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>
    <script src="js/plugins.js"></script>
    <script src="js/theme.js"></script>
</body>

</html>